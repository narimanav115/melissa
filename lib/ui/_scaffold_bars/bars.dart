import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:melissa_pharm/theme/styles.dart';
import 'package:melissa_pharm/ui/main_page/catalog.dart';
import 'package:melissa_pharm/ui/main_page/profile.dart';
import 'package:melissa_pharm/ui/secondary_pages/cart_page.dart';
import 'package:melissa_pharm/ui/widgets/buttons/action_button.dart';

part 'appbar.dart';
part 'drawer.dart';