part of 'bars.dart';

class DrawerCustom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double topAppPadding = MediaQuery.of(context).padding.top + kToolbarHeight + 1;
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 150,
              child: Center(child: Image.asset('assets/logo.png', scale: 4,))
          ),
          StyledTile(icon: Icons.category_outlined, text: 'Каталог', onTap: (){
            Navigator.of(context ).push(MaterialPageRoute(builder: (context)=>Catalog()));
          },),
          StyledTile(icon: Icons.person_outline_sharp, text: 'Профиль', onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ProfilePage()));},),
          StyledTile(icon: Icons.disc_full_outlined, text: 'Скидки'),
          StyledTile(icon: Icons.location_on_outlined, text: 'Аптеки'),
          StyledTile(icon: Icons.headset_mic_outlined, text: 'Служба поддержки'),
          StyledTile(icon: Icons.mail_outline_outlined, text: 'Контакты'),
          StyledTile(icon: Icons.assignment_outlined, text: 'Конфидициальность'),
        ]
      ),
    );
  }
}

class StyledTile extends StatelessWidget {
  final IconData icon;
  final String image;
  final String text;
  final String endText;
  final onTap;

  const StyledTile({Key key,this.icon,@required this.text, this.image, this.endText,@required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,

      child: Column(
        children: [
          Divider(height: 1),
          Container(
            height: 60,
            color: Colors.black12.withOpacity(0.01),
            padding: EdgeInsets.only(left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(children: [
                  if(image!=null)Image.asset(image),
                  if(icon!=null)Icon(icon),
                  SizedBox(width: 13,),
                  Text(text),
                ],),
                if(endText!=null)Container(
                  padding: EdgeInsets.only(right: 16),
                  child: Row(
                    children: [
                      Text(endText),
                    ],
                  ),
                )
              ],
            ),
          ),
          Divider(height: 1,),
        ],
      ),
    );
  }
}
