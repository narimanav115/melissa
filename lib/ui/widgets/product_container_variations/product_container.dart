import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:melissa_pharm/theme/styles.dart';
import 'package:melissa_pharm/ui/widgets/buttons/action_button.dart';

class ProductContainer extends StatelessWidget {
  final bool hasDiscount;

  const ProductContainer({Key key, this.hasDiscount=false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
        },
        child: Container(
          // padding: EdgeInsets.symmetric(horizontal: 16),
          child: Stack(
            children: [
              Flex(
                direction: Axis.vertical,
                children: [
                  SizedBox(height: 12),
                  Container(
                    height: 140,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/product.png',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Container(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 18, vertical: 4),
                          child: Container(
                            child: Text(
                              'Паста для шугаринга SugarLife Плотная, 200 г',
                              softWrap: true,
                              maxLines: 3,
                              style: SugarLifeTheme.regular,
                            ),
                          ),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                    ),
                  ),
                  Container(
                    height: 40,
                    child: GestureDetector(
                      onTap: () {

                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('140 00 Т', style: SugarLifeTheme.regularBold,),
                           if(hasDiscount==true) Icon(
                              Icons.arrow_forward_sharp
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  // if(hasDiscount==false)ActionButton( icon: SugarIcons.shop_cart,label: 'Добавить в корзину', onPressed: (){
                  //   print('added to cart');
                  // })
                ],
              ),
                Positioned(
                    top: 8,
                    left: 12,
                    child: LikeButton(
                    )),
            ],
          ),
        ),
      ),
    );
  }
}

class BigSizeContainer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {

        },
        child: Container(
          // padding: EdgeInsets.symmetric(horizontal: 16),
          child: Stack(
            children: [
              Flex(
                direction: Axis.vertical,
                children: [
                  SizedBox(height: 12),
                  Container(
                    height: 200,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          'assets/product.png',
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Container(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 18, vertical: 4),
                          child: Container(
                            width: 220,
                            child: Text(
                              'Паста для шугаринга SugarLife Плотная, 200 г',
                              softWrap: true,
                              maxLines: 3,
                              textAlign: TextAlign.center,
                              style: SugarLifeTheme.regular,
                            ),
                          ),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                    ),
                  ),
                  Container(
                    height: 40,
                    child: GestureDetector(
                      onTap: () {

                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('140 000 Т', style: SugarLifeTheme.regularBold,),
                            Text('В наличии')
                          ],
                        ),
                      ),
                    ),
                  ),

                ],
              ),
              Positioned(
                  top: 8,
                  left: 12,
                  child: LikeButton(
                  )),
            ],
          ),
        ),
      ),
    );
  }
}


class LikeButton extends StatelessWidget {
  final int id;
  LikeButton({this.id,});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
      },
      child:       Icon( Icons.favorite_border,color: Colors.red,size: 30,),

    );
  }
}
