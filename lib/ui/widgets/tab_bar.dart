import 'package:flutter/material.dart';
import 'package:melissa_pharm/theme/styles.dart';

class CategoriesTabs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        DefaultTabController(
          length: 3,
          child: Column(
            children: [
              Container(
                height: 50,
                child: CustomTabBar(),
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Container(
                  height: 200,
                  child: TabBarView(
                    children: [
                      Text('Шикарная линейка на основе белой ангарской глины и леофилизированного кокосового молочка SUGAR LIFE “WHITE GOLD” Абсолютно гипоаллергенный продукт уникальной консистенции. Очень пластичная и мягкая, при этом не тает и не растекается, что обеспечивает невероятное удобство ра'),
                      Icon(Icons.directions_transit),
                      Icon(Icons.directions_bike),
                    ],
                  ),
                ),
              )

            ],
          ),
        ),
      ],
    );
  }
}

class CustomTabBar extends StatefulWidget {
  @override
  _CustomTabBarState createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar> {

  final List<String> tabsName=['Описание','Состав','Применение'];


  @override
  Widget build(BuildContext context) {
    return Container(
      child: TabBar(
        isScrollable: true,
        onTap: (index){
        },
        unselectedLabelColor: Colors.black,
        labelColor: Colors.black,
        indicatorPadding: EdgeInsets.zero,
        labelPadding: EdgeInsets.symmetric(horizontal: 0.1),
        indicator: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(
                    0, 1), // changes position of shadow
              ),
            ],
            border: Border.all(color: SugarLifeTheme.yellow, width: 2),
            color: Colors.white),
        tabs: tabsName.map((e) =>
            Container(
              // decoration: BoxDecoration(
              //     border: Border.symmetric(vertical: BorderSide(color: Colors.white, width: 2,), horizontal: BorderSide(color: Colors.white, width: 2,))),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Center(
                      child: new Text(e)),
                ))).toList(),
      ),
    );
  }
}

