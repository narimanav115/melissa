import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



abstract class SugarLifeTheme{
  static TextStyle bigStyle = TextStyle(fontFamily: 'Bebas Neue',fontSize: 30, fontWeight: FontWeight.w700);
  static TextStyle regular = TextStyle(fontFamily: 'Lato',fontSize: 15, fontWeight: FontWeight.w300,);
  static TextStyle regularBold = TextStyle(fontFamily: 'Lato',fontSize: 13, fontWeight: FontWeight.w600,);
  static TextStyle whiteText = TextStyle(fontFamily: 'Avanti',fontSize: 16, fontWeight: FontWeight.w400, color: Colors.white);
  static TextStyle buttonStyle = TextStyle(fontFamily: 'Avanti',fontSize: 14, fontWeight: FontWeight.w400);
  static Color grey = Color(0xFFE8F5E2);
  static Color yellow = Color(0xFFFFE24B);

  static Color green = Color(0xFF59B439);
}

// class BebasStyle implements SugarLifeTheme{
//
// }
//
// class Avanti implements SugarLifeTheme{
//
// }
// class Lato implements SugarLifeTheme{
//
// }


class LoadingCircle extends StatelessWidget {
  final Color color;

  const LoadingCircle({Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(color??Colors.white),
      ),
    );
  }
}
// class LoadingCircleGreen extends StatelessWidget {
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 20,
//       width: 20,
//       child: CircularProgressIndicator(
//         valueColor: new AlwaysStoppedAnimation<Color>(SugarLifeTheme.green),
//       ),
//     );
//   }
// }
