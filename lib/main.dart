import 'package:flutter/material.dart';
import 'package:melissa_pharm/ui/main_page/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.white,
        fontFamily: 'SF',
        backgroundColor: Colors.transparent,
        canvasColor: Colors.white,
        // accentColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      title: 'Melissa pharmacy',
      home: HomePage(),
    );
  }
}

